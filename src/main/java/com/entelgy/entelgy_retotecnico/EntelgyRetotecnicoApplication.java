package com.entelgy.entelgy_retotecnico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntelgyRetotecnicoApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntelgyRetotecnicoApplication.class, args);
	}

}
